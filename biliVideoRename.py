###     version 1.0     ###

# import fileOperate
import getInfo
import hint

import os
import os.path as Path

from time import sleep

# 主程序 Main
# 设置工作目录
rootdir = input('请设置视频所在的文件夹(如果直接回车则默认选择当前目录)')
if rootdir == '':
    rootdir = os.getcwd()
ignore = '.git'

# 2. 对该文件夹下的所有 ["audio.m4s", "video.m4s"] 文件进行全部合并, 进行 hint-track 处理
hint.hint_all(rootdir)

# 3. 遍历所有目录, 然后再重命名所有相关文件
for root,dirs,files in os.walk(rootdir):
    for dir in dirs:
        print(Path.join(root, dir))
        if os.path.join(dir) in ignore:
            print('撞到忽略目录了，跳过 >v<')
            continue
        else:
            os.chdir(os.path.join(root, dir))
            # 3-1. 对子文件夹的内容进行改名
            getInfo.rename_files()
            # 3-2. 对子文件夹的所有 ["audio.m4s", "video.mps"] 进行删除
            # fileOperate.del_mp4s()
            sleep(0.05)

exit()