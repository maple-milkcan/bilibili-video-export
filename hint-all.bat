:: version 1.0
@echo off
title 哔哩哔哩番剧批量混流 HintTrack
echo 即将对此文件夹下的所有子文件夹进行混流（任意键确认继续）
pause >nul
for /d %%d in (*) do (
echo 进入 %%d 文件夹处理
if exist "%%d\video.m4s" (
MP4Box.exe -add "%%d\video.m4s" -add "%%d\audio.m4s" -new "%%d\muxed.mp4"
MP4Box.exe "%%d\muxed.mp4" -keepall -inter 0 -isma -hint -no-iod -no-sys
)
echo.
)
echo 处理完成
pause