## 哔哩哔哩动画第三方 UWP 视频合并工具

### 介绍
1. 本程序由 Python 语言编写
2. 适配了哔哩哔哩第三方UWP（作者：[逍遥橙子](https://github.com/xiaoyaocz)）的视频下载文件结构，可实现批量视频合并功能

### 使用说明
1. 本程序需要依赖 json 模块，如果源码运行过程中出现报错，可尝试更新 Python 版本至最新，或在 cmd 命令行中输入 `pip install json`
2. 本程序还需要依赖 MP4Box.exe 等组件，如需使用可以把整个工程克隆下来然后运行
3. 本作者短期内不会发布编译好的独立exe文件或安装包，程序还在测试过程中，请见谅
2. 主程序源码在 [biliVideoRename.py](/biliVideoRename.py)，请在该文件中调试运行

### 更新日志
#### v1.0
**final ver**&emsp;*2022-01-26&emsp;updated*
* 已修复 hint-all.bat 条件检测错误导致无法运行的问题
* 已修复 hint 模块中无法找到路径的问题
* 已添加 hint 模块中把依赖文件复制到目标目录，并会在批处理完成后自动删除
* 已添加 fileOperate 模块（可选）

**dev 03**&emsp;&emsp;*2022-01-26&emsp;updated*
* 支持可更改工作目录的路径
* 同时 hint 模块也支持传入工作目录路径
* bat 批处理相关已测试完毕，hint-all 的内容已完善
* MP4Box 等相关组件已装载

**dev 02**&emsp;&emsp;*2022-01-26&emsp;updated*
* 模块化设计，设计逻辑清晰
* 支持遍历文件夹下的所有子文件夹，并逐一进入进行操作
* 支持将 ["video.mp4", "audio.m4a"] 进行混流合并
* 支持对混流后的视频进行 hint-track 处理
* 重命名失败或找不到文件时会弹出错误提示

**dev 01**&emsp;&emsp;*2022-01-25&emsp;updated*
* 测试开发阶段版本，功能待完善