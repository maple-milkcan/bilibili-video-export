###     version 1.0     ###

import json
import os

# 功能: 获取info.json的值并对该文件夹下的文件进行改名
def rename_files():
    # 读取json下的标题键值
    filename = 'info.json'
    title = ''
    try:
        with open(filename, 'r', encoding='UTF-8') as f:
            info_list = json.load(f)
            print(info_list)
            title = info_list['EpisodeTitle']
            print(title)
    except Exception as e:
        print(e)
        print('找不到文件', filename, '或该文件被占用')
        return
    else:
        print('打开成功')

    # 重命名文件
    # 重命名弹幕文件
    try:
        os.rename('danmaku.xml', title + '.xml')
    except Exception as e:
        print(e)
        print('弹幕文件重命名失败')
    else:
        print('弹幕文件重命名成功')
    # 重命名视频文件
    try:
        os.rename('muxed.mp4', title + '.mp4')
    except Exception as e:
        print(e)
        print('视频文件重命名失败')
    else:
        print('视频文件重命名成功')
    # 重命名字幕文件
    try:
        os.rename('中文（繁体）.json', title + '.json')
    except Exception as e:
        print(e)
        print('字幕文件重命名失败')
    else:
        print('字幕文件重命名成功')

    try:
        os.rename('中文（简体）.json', title + '.json')
    except Exception as e:
        print(e)
        print('字幕文件重命名失败')
    else:
        print('字幕文件重命名成功')

    return
