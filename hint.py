###     version 1.0     ###

import os
from time import sleep

# 功能: 先预处理, 对文件夹下的所有文件进行混流, hint-track
def hint_all(currentdir):
   currentdir += '\\'
   cmdbat = 'hint-all.bat'

   # 复制文件到目标目录
   os.system('copy hint-all.bat ' + "\"" + currentdir + "hint-all.bat \"")
   os.system('copy js.dll ' + "\"" + currentdir + "js.dll \"")
   os.system('copy libeay32.dll ' + "\"" + currentdir + "libeay32.dll \"")
   os.system('copy mp4box.exe ' + "\"" + currentdir + "mp4box.exe \"")
   os.system('copy ssleay32.dll ' + "\"" + currentdir + "ssleay32.dll \"")
   
   os.chdir(currentdir)
   # currentdir = currentdir.replace('\\', '/')
   
   # os.startfile("hint-all.bat")
   command = 'cd ' + "\"" + currentdir + "\""
   print(command)
   os.system(command)
   os.system(cmdbat)

   # 完成后删除文件
   os.system('del hint-all.bat')
   os.system('del js.dll')
   os.system('del libeay32.dll')
   os.system('del mp4box.exe')
   os.system('del ssleay32.dll')
   sleep(20)
   return