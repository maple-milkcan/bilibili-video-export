###     version 1.0     ###

import os

# 可选功能: 对文件夹内的所有 ["video.m4s", "audio.m4s"] 进行改名操作
def stream_to_mp4 ():
    try:
        os.rename('audio.m4s', 'audio.m4a')
    except Exception as e:
        print(e)
        print('音频流重命名失败')
    else:
        print('音频流重命名成功')
    
    try:
        os.rename('video.m4s', 'video.mp4')
    except Exception as e:
        print(e)
        print('视频流重命名失败')
    else:
        print('视频流重命名成功')
    return

# 可选功能: 混流完成后, 对文件夹内的所有 ["video.mp4", "audio.m4a"] 进行删除操作
def del_mp4s ():
    os.system('del video.mp4')
    os.system('del audio.m4a')
    return
   